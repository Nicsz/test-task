## Requirements

- Laravel 5.8
- PHP 7.3
- npm 6.13.4

## Installing

<pre>
git clone https://gitlab.com/Nicsz/test-task
<span class="pl-c1">cd</span> test-task
composer install
npm install
</pre>