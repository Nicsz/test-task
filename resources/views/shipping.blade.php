@extends('layouts.layout')

@section('content')

  <div class="shipping">
    <div class="container">
      <form class="form" action="{{ redirect('/') }}" name="form">
        {{ csrf_field() }}

        <div class="form-group">
          <label class="form-label" for="inputName">Name*</label>
          <div class="wrapper">
            <input type="text" class="form-input" id="inputName" name="name" value="" pattern="^[A-Za-z]+$" required />
            <div class="error" id="error_inputName"></div>
           {{-- @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror--}}
          </div>
        </div>
        <div class="form-group">
          <label class="form-label" for="inputAddress">Address*</label>
          <div class="wrapper">
            <input type="text" class="form-input" id="inputAddress" name="address" value="" required />
            <div class="error" id="error_inputAddress"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label" for="inputPhone">Phone</label>
          <div class="wrapper">
            <input type="tel" class="form-input" id="inputPhone" name="phone" value="" />
            <div class="error" id="error_inputPhone"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label" for="inputEmail">Email</label>
          <div class="wrapper">
            <input type="email" class="form-input" id="inputEmail" name="email" value="" />
            <div class="error" id="error_inputEmail"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label" for="selectOptions">Shipping options</label>
          <div class="wrapper">
            <select class="form-input" id="selectOptions">
              <option selected>

                {{ ((int)$totalSum > 300) ? 'Free express shipping' : 'Free shipping' }}

              </option>
              <option>Express shipping - additional 9.99 €</option>
              <option>Courier shipping - additional 19.99 €</option>
            </select>
            <i class="fa fa-caret-down arrow-down" aria-hidden="true"></i>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="form-pay" id="btnPay">Pay</button>
        </div>
      </form>
    </div>
  </div>

@endsection