@extends('layouts.layout')

@section('content')

  <div class="container">
    <div class="cart">

      @if(empty($products))

        <div class="cart-empty">
          Empty cart
        </div>

      @endif

      @foreach($products as $product)

        <div class="cart-item item{{ $product->id }}">
          <div class="wrapper-info">
            <img src="{{ asset('/img/default-1.png') }}" alt="item-image" class="item-image">
            <div class="item-info">
              <h4 class="item-info-title">
{{--                Lorem ipsum dolor sit amet--}}
                {{ $product->title }}
              </h4>
              <div class="item-info-text">
                {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab animi assumenda at blanditiis commodi
                cupiditate eligendi eveniet excepturi minima, natus nostrum odit omnis quo quos ratione recusandae
                repudiandae. A.--}}
                {{ $product->description }}
              </div>
            </div>
          </div>
          <div class="wrapper-action">
            <div class="item-quantity">
              <button class="item-btn btn-minus" id="minus" type="button" name="minus">
                <img class="item-btn__icon-minus" src="{{ asset('/img/minus.svg') }}" alt="minus" />
              </button>
              <input class="item-quantity-input quantity" id="quantity{{ $product->id }}" type="number" name="name" value="1" readonly>
              <button class="item-btn btn-plus" id="plus" type="button" name="plus">
                <img class="item-btn__icon-plus" src="{{ asset('/img/plus.svg') }}" alt="plus" />
              </button>
            </div>
            <div class="item-price" id="price{{ $product->id }}">
{{--              75.00&nbsp;$--}}
              {{ number_format($product->price, 2, '.',' ') }}&nbsp;€
            </div>
            <div class="item-trash">
              <form action="/cart/{{ $product->id }}" method="POST">
                {{ csrf_field() }}
                {!! method_field('delete') !!}

                <button type="submit" class="item-trash-button"><i class="far fa-trash-alt"></i></button>
              </form>
            </div>
          </div>
        </div>

      @endforeach

    </div>

    <div class="result">
      <div class="result-price" id="totalSum"></div>
      <form action="{{ route('shipping') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="priceS" id="priceS">
        <button type="submit" class="result-btn" id="buy">Buy</button>
      </form>
    </div>
  </div>

@endsection