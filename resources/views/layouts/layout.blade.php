<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Normalize -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">

  <!-- CSS -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <title>Laravel</title>

  <style>
    body {
      font-family: 'Roboto', sans-serif;
    }
  </style>
</head>
<body>

  @yield('content')

  <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
</body>
</html>