<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    //
  public function get() {

    $products = [
      0 => ['id' => 1, 'title' => 'Audi', 'description' => 'Deutsche auto', 'price' => '300'],
      1 => ['id' => 2, 'title' => 'Audi', 'description' => 'Deutsche auto', 'price' => '200'],
      2 => ['id' => 3, 'title' => 'Audi', 'description' => 'Deutsche auto', 'price' => '500'],
      3 => ['id' => 4, 'title' => 'Audi', 'description' => 'Deutsche auto', 'price' => '400'],
      4 => ['id' => 5, 'title' => 'Audi', 'description' => 'Deutsche auto', 'price' => '100'],
    ];

    $products = json_encode($products);

    return $products;
  }
}
