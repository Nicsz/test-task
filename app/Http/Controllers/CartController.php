<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CartController extends Controller
{
  public function index() {
    $client = new Client();

    $request = $client->get('https://5f11c995d5e6c90016ee4c4f.mockapi.io/api/products');
    $response = $request->getBody();
    $response = json_decode($response);

    /*$client = new Client();

    $request = $client->get('http://cart/resource');
    $response = $request->getBody();
    $response = json_decode($response);*/

//    dd($response);

    return view('cart', ['products' => $response]);
  }

  public function destroy($id) {
    $client = new Client();
    $client->delete('https://5f11c995d5e6c90016ee4c4f.mockapi.io/api/products/' . $id);

    return redirect('/cart');
  }
}
