"use strict";

/* Function which check on which counter of product will be a click */

function addCounter(count) {
  let minus = count.querySelector("#minus");
  let plus = count.querySelector("#plus");
  let quantity = count.querySelector(".quantity");


  plus.addEventListener("click", function () {
    if (quantity.value < 50)
      quantity.value++;
    else
      return 0;

    location.reload();
  });

  minus.addEventListener("click", function () {
    quantity.value--;
    if (quantity.value <= 1) {
      quantity.value = 1;
      // return 0;
    }

    location.reload();
  });
}


let products = document.querySelectorAll(".cart-item");

products.forEach(addCounter);

/* Count total price */

let price, quantity;
let priceForSend = document.getElementById("priceS");
let buy = document.getElementById("buy");
let totalSum = document.getElementById("totalSum");
let sum = 0;

for (let i = 1; i <= products.length; i++) {
  price = document.getElementById("price" + i);
  quantity = document.getElementById("quantity" + i);

  sum += parseInt(quantity.value) * parseFloat(price.innerText).toFixed(2);
}

/* If these variables exists */

if (totalSum && buy) {
  totalSum.innerText = sum.toFixed(2) + " €";

  buy.addEventListener("click", function () {
    priceForSend.setAttribute('value', sum);
  })
}


/* Validation Form */


let input = document.querySelectorAll('input');

for (let i = 0; i < input.length; i++) {
  let error = document.getElementById('error_' + input[i].getAttribute('id'));

  input[i].onblur = function () {

    if (this.getAttribute('id') === 'inputName') {
      let x = document.form.inputName.value;

      validateInputs(this.getAttribute('id'), x, /^[A-Za-z]+$/);
    }

    if (this.getAttribute('id') === 'inputAddress') {
      let x = document.form.inputAddress.value;

      let reg = /^[A-Za-z0-9/,]+$/;
      if (reg.test(x) && (x.length > 10 && x.length <= 50)) {
        this.classList.add('valid');
      } else {
        this.classList.remove('valid');
        this.classList.add('invalid');

        error.innerHTML = 'Invalid address';

        document.getElementById("btnPay").disabled = true;
      }
    }

    if (this.getAttribute('id') === 'inputPhone') {
      let x = document.form.inputPhone.value;

      validateInputs(this.getAttribute('id'), x, /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,8}$/);
    }

    if (this.getAttribute('id') === 'inputEmail') {
     let x = document.form.inputEmail.value;

     validateInputs(this.getAttribute('id'), x, /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z\-])+\.)+([a-zA-Z]{2,4})+$/);
    }
  };

  function validateInputs(id, value, regular) {

    id = (id.replace(/^.{5}/, '')).toLowerCase();

    if (value.match(regular)) {
      input[i].classList.add('valid');
    } else {
      input[i].classList.remove('valid');
      input[i].classList.add('invalid');

      error.innerHTML = 'Invalid ' + id;

      document.getElementById("btnPay").disabled = true;
    }
  }

  input[i].onfocus = function() {
    if (this.classList.contains('invalid')) {
      // Delete error indicator, as user want to enter data again
      this.classList.remove('invalid');
      this.classList.add('valid');
      error.innerHTML = "";
      document.getElementById("btnPay").disabled = false;
    }
  };
}