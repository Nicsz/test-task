<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @var  \Illuminate\Routing\Router $router */

$router->get('/', function () {
  return view('welcome');
});

$router->get('/cart', "CartController@index")->name('index');
$router->delete('/cart/{product}', "CartController@destroy")->name('delete');

$router->post('/shipping', "ShippingController@index")->name('shipping');
$router->get('/resource', "ResourceController@get")->name('resource');
